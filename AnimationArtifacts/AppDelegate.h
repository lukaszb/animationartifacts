//
//  AppDelegate.h
//  AnimationArtifacts
//
//  Created by Łukasz Balcerzak on 11/4/12.
//  Copyright (c) 2012 Łukasz Balcerzak. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSView *wrapper;

- (IBAction)onEnter:(NSTextField *)sender;

@end
