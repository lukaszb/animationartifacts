//
//  RoundedView.h
//  SimpleLogger
//
//  Created by Łukasz Balcerzak on 2/21/12.
//  Copyright (c) 2012 lukaszbalcerzak@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RoundedView : NSView

@end
