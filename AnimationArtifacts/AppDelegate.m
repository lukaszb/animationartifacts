//
//  AppDelegate.m
//  AnimationArtifacts
//
//  Created by Łukasz Balcerzak on 11/4/12.
//  Copyright (c) 2012 Łukasz Balcerzak. All rights reserved.
//

#import "AppDelegate.h"
#import "FallingTextView.h"

@implementation AppDelegate

@synthesize wrapper = _wrapper;

- (IBAction)onEnter:(NSTextField *)sender
{
    NSLog(@"%@", [sender stringValue]);
    
    NSString *keystroke = [sender stringValue];
    NSRect wrapperFrame = [self.wrapper frame];
    
    NSRect frame = wrapperFrame;
    
    NSAttributedString *attributedString = [FallingTextView attributedString:keystroke];
    frame.size.height = attributedString.size.height;
    
    frame.origin.y = wrapperFrame.origin.y + wrapperFrame.size.height;
    FallingTextView *textView = [[FallingTextView alloc] initWithFrame:frame text:attributedString];
    
    [self.window.contentView addSubview:textView];
    
    frame.origin.y -= wrapperFrame.size.height + frame.size.height;
    
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:1.5f];
    [textView.animator setFrame:frame];
    [textView.animator setAlphaValue:0.0];
    [NSAnimationContext endGrouping];
    
    if ([textView.animator isAnimating]) {
        NSLog(@"%@ is animating!", textView);
    }
    
}

@end
