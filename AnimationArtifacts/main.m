//
//  main.m
//  AnimationArtifacts
//
//  Created by Łukasz Balcerzak on 11/4/12.
//  Copyright (c) 2012 Łukasz Balcerzak. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
