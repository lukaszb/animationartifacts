//
//  FallingTextView.m
//  AnimationArtifacts
//
//  Created by Łukasz Balcerzak on 11/4/12.
//  Copyright (c) 2012 Łukasz Balcerzak. All rights reserved.
//

#import "FallingTextView.h"

@implementation FallingTextView

@synthesize attributedString = _attributedString;

-(id)initWithFrame:(NSRect)frameRect text:(NSAttributedString *)attributedString;
{
    if (self = [super initWithFrame:frameRect]) {
        self.attributedString = attributedString;
    }
    return self;
}

- (void)setAttributedString:(NSAttributedString *)attributedString
{
    if (_attributedString != attributedString) {
        _attributedString = attributedString;
    }
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self drawString];
}

- (void)drawString
{
    [_attributedString drawAtPoint:NSMakePoint([self textOriginX], 0)];
}

- (int)textOriginX
{
    int x = self.frame.size.width / 2;
    x -= _attributedString.size.width / 2;
    return x;
}

+ (NSAttributedString *)attributedString:(NSString *)text
{
    NSFont *font = [NSFont fontWithName:@"Menlo" size:36];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSCenterTextAlignment];
    
    NSDictionary *attributes = @{
        NSFontAttributeName: font,
        NSBackgroundColorAttributeName: [NSColor clearColor],
        NSForegroundColorAttributeName: [NSColor whiteColor],
        NSParagraphStyleAttributeName: style,
    };
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes: attributes];
    
    return attributedText;
}

@end
