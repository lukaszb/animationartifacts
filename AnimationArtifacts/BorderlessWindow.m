//
//  NiceWindow.m
//  SimpleLogger
//
//  Created by Łukasz Balcerzak on 1/27/12.
//  Copyright (c) 2012 lukaszbalcerzak@gmail.com. All rights reserved.
//

#import "BorderlessWindow.h"

@implementation BorderlessWindow

- (id)initWithContentRect:(NSRect)contentRect
                styleMask:(NSUInteger)windowStyle
                  backing:(NSBackingStoreType)bufferingType
                    defer:(BOOL)deferCreation
{
    self = [super
            initWithContentRect:contentRect
            styleMask:NSBorderlessWindowMask
            backing:bufferingType
            defer:deferCreation];
    if (self)
    {
        [self setAlphaValue:0.9];
        [self setOpaque:NO];
        [self setBackgroundColor:[NSColor clearColor]];
    }
    return self;
}


- (BOOL)canBecomeKeyWindow
{
    return YES;
}

- (BOOL)canBecomeMainWindow
{
    return YES;
}

@end
