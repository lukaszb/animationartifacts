//
//  RoundedView.m
//  SimpleLogger
//
//  Created by Łukasz Balcerzak on 2/21/12.
//  Copyright (c) 2012 lukaszbalcerzak@gmail.com. All rights reserved.
//

#import "RoundedView.h"
#import <QuartzCore/QuartzCore.h>
#define RADIUS 5.0

@implementation RoundedView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)awakeFromNib
{
    self.wantsLayer = YES;
    self.layer = [CALayer layer];
    self.layer.backgroundColor = [[NSColor blackColor] CGColor];
    self.layer.cornerRadius = RADIUS;
}

@end
