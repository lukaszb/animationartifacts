//
//  FallingTextView.h
//  AnimationArtifacts
//
//  Created by Łukasz Balcerzak on 11/4/12.
//  Copyright (c) 2012 Łukasz Balcerzak. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FallingTextView : NSView

+ (NSAttributedString *)attributedString:(NSString *)text;

-(id)initWithFrame:(NSRect)frameRect text:(NSAttributedString *)text;

@property (nonatomic, strong) NSAttributedString* attributedString;

@end
