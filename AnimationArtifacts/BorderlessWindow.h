//
//  NiceWindow.h
//  SimpleLogger
//
//  Created by Łukasz Balcerzak on 1/27/12.
//  Copyright (c) 2012 lukaszbalcerzak@gmail.com. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface BorderlessWindow : NSWindow

@end
